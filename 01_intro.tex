\section{Introduction}\label{sec:intro}

Recent years have seen a surge of interest in staging and related 
metaprogamming techniques, driven by the widening gap between
high-level languages and emerging hardware platforms. 
In the context of Scala, The LMS (Lightweight Modular Staging) \cite{rompf10lms,rompf12cacm} framework has seen applications
in DSLs for heterogeneous parallelism (Delite \cite{pact11delite,rompf11dsl,micro11lee,sujeeth2013composition}),
machine learning (OptiML \cite{icml11optiml}), 
library generation for numeric kernels (Spiral \cite{DBLP:conf/gpce/OfenbeckRSOP13,DBLP:conf/pldi/StojanovORP14}), 
hardware generation \cite{DBLP:conf/fpl/GeorgeLNRBSOOI14,DBLP:conf/fpt/GeorgeNROI13}, 
SQL database engines \cite{DBLP:journals/pvldb/KlonatosKRC14,rompf2015queries}, protocol and data format 
parsers \cite{DBLP:conf/oopsla/JonnalageddaCSRO14}, and recently for \emph{safe} systems
development by generating verifiable C code with contracts
in a specification language \cite{lmsverify}.

% requirements: 
% - need to do this at runtime. for a large system we don't have
%   all required information at compile time (think SQL queries)
% - need to be programmable. expose stage distinction

With such a broad variety of applications, spanning
code generation targets from JavaScript, Scala, C/C++, CUDA, 
all the way to Verilog, the desire to change different
aspects of LMS has come up from time to time. 
However, most such change attempts that arose in a given
use-case have turned out to break key properties 
necessary for at least one other key use-case of LMS.
Hence, the design of LMS as it has evolved seems to be 
a local optimum --- but it seems hard to believe that this 
particular point in the design space should be globally
optimal, in particular since there are recurring pain 
points, for example repetitive boilerplate for DSL 
definitions and excessive Scala compile times.


\begin{figure}
\includegraphics[width=\columnwidth]{figures/sec1-bottleneck.pdf}
\caption{\label{fig:bn}General-purpose compiler vs DSL pipeline}
\end{figure}

% Change attempts over the years
What are then the constraints on the design space?
We identify five key aspects that make LMS what it is
and which we deem essential for embedded DSL compiler 
frameworks to achieve applicability on a similar broad
scale as LMS:

\begin{sitemize}
\item 
\textbf{Type-based embedding} enables mixing
present-stage and future-stage computations seamlessly
in a single program, while keeping the stages
stratified. Type-based embedding further enables
us to \emph{abstract} over the stage.

\item 
\textbf{True multi-stage semantics} guarantees, in 
constrast to syntactic template expansion, that evaluation order
\emph{within} each stage follows the normal call-by-value
rules. 

% expose stage distinction, 
% virtualization of host language,
% multi-stage semantics: maintain eval order

\item 
\textbf{Graph-based IR},
where computations can move freely, enables sophisticated 
and generic optimizations, implemented once and for all, 
for many DSLs.

\item
\textbf{Horizontal extensibility} enables users to
add custom IR nodes and optimizations, based on eager
exhaustive rewriting, to an existing language definition.

\item
\textbf{Vertical extensibility} enables users to
add analysis and transformation passes, potentially
introducing new intermediate languages.
Such transformations are implemented as \emph{staged interpreters}.
\end{sitemize}



Together, these facilities enable high-bandwidth DSL compiler
pipelines as shown in Figure~\ref{fig:bn}. On the left-hand side
we illustrate a general-purpose compiler, whose generic abstractions 
like objects, modules, or higher-order functions, can be \emph{used} 
to implement domain-specific abstractions like matrices or graphs,
but do not allow the compiler to \emph{reason} about such
domain-specific objects, which leads to a semantic bottleneck.

On the right-hand side we show a DSL compiler pipeline consisting
of multiple IR levels. At the top, we have a considerable variety
of DSLs or domain specific abstractions, e.g.\ an IR that models 
matrices, graphs, or queries in relational algebra, 
with a corresponding set of domain-specific optimizations, e.g.\
rewrites that exploit algebraic identities. 
Once these optimizations are applied exhaustively, the program 
is \emph{lowered} to the next abstraction level, for example,
arrays and while loops. On this level, there is a different set
of optimizations (e.g.\ tiling, loop fusion), which is again
applied exhaustively before lowering to a variety of hardware-specific
targets. To avoid the bottleneck of a general purpose compiler,
it is important that the stack can be extended vertically,
for example by adding new front-ends, back-ends, or IR levels 
in the middle, and also that the IR on each level can be extended 
horizontally with new data types and optimizations. 

Finally, generic optimizations
such as dead code elimination or common subexpression elimination
are equally important as domain-specific optimizations, and they
need to be available on all levels of the stack.
With such an architecture, DSL compilers are easy to construct 
from existing stacks, by horizontal or vertical extension.

% TODO
% \note{
% - Fig. 1 didn't do anything for me.
% - The five "key aspects" of LMS cannot be understood on a high level given 
%   the information in Sec. 1. They become quite clear later on, but the summary 
%   in Sec. 1 is not self-contained.}



Note that even though LMS primarily targets \emph{embedded}
DSLs, it is easy to add textual front-ends, as has been done,
for example, with regular expressions \cite{rompf12optimizing} 
and SQL \cite{rompf2015queries}.



Based on these observations, this paper makes the following
contributions:

\begin{sitemize} 

\item 
We sketch the design space of LMS as outlined above
and review current design decisions
(Section~\ref{sec:zero}).

\item 
We discuss issues that frequently arise in practice
(Section~\ref{sec:problems}).

\item 
We describe an alternative type-based embedding, based 
on type classes instead of higher-kinded types
(Section~\ref{sec:one}).

\item
We discuss using Scala macros for certain aspects of the
embedding that have previously been implemented in a compiler fork
(Section~\ref{sec:two}).

\item 
We present an alternative definition of a graph-based IR 
which is untyped,
and we discuss how to still achieve type-safe rewritings 
and transformations
(Section~\ref{sec:three}).


% \item 
% ?
% (Section~\ref{sec:three}).

\end{sitemize}

\noindent
It is important to note that these patterns have 
been known for some time, and that they reflect
the work of a large number of people from 
different institutions (EPFL, ETH, Huawei, Stanford, and Purdue).
The main contribution of this paper is to 
document and crystallize them.
Section~\ref{sec:related} surveys generic
related work, and
Section~\ref{sec:acks} provides
further attribution for the work described
in this paper.



