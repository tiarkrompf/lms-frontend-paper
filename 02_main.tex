%\input{02a_figs}
%\input{02b_figs}


\section{The LMS Design Space}\label{sec:zero}

We discuss the five key elements of LMS in more
depth, and describe the design decisions taken
by the current implementation.


\subsection{Type-Based Embedding}

% - Sec. 2.1: Don't your arguments apply only to online PE? How does offline PE 
%   fit into your spectrum and how does it relate to LMS?


The essence of multi-stage programming is that we
can operate with present-stage and future-stage
values in the same program. But how shall we
distinguish the two?

There is a whole spectrum of techniques which make
the distinction more or less prominently visible
in the program,
which we can classify according to their \emph{noise level}.
This characterization is inspired by a remark of
Bjarne Stroustrup \cite{stroustrup}: 
``People confuse the familiar for the simple. 
For new features, people insist on LOUD explicit syntax. 
For established features, people want terse notation.''

\medskip

\begin{center}
\makebox[0.8\columnwidth]{
\includegraphics[height=1em]{figures/speaker-loud.pdf}
\quad {\Large |} \hrulefill\ |\ \hrulefill\ {\tiny\bf |} \quad
\includegraphics[height=1em]{figures/speaker-silent.pdf}
\hspace{0ex}$^{\!\color{black}\textsf{\textbf{x}}}$}\\
\makebox[0.71\columnwidth]{
\small\textsf{Quotation \hfill Type-Based \hfill \qquad PE}}
\end{center}

Syntactic quotations on the one end make the stage
distinction obvious, but introduce clutter and are thus
somewhat cumbersome to use. Traditional
partial evaluation \cite{jones93partial} does not distinguish the
stages in any observable way, but relies on automatic
binding-time analysis to separate the program into
static and dynamic components, or on automatic online
specialization \cite{DBLP:conf/fpca/WeiseCRS91}. 
Either method of fully automatic separation is brittle 
in practice, and programmers have little control over 
the process, and in particular little effective means 
of debugging.

A similar noise level comparison can be drawn for
typed vs untyped languages and explicit type annotations
vs inferred types, with well-known pro and contra
arguments about reading code vs writing code,
refactoring, types as documentation, and so on.

A key design decision of LMS is to treat the stage
distinction in exactly the same way as ordinary types,
and use the meta-language
type system to distinguish present-stage from future-stage
expressions. This strikes a good balance in practice,
reducing syntactic noise to a comfortable volume,
consistent with the normal use of type annotations in
the host language, but at the same time keeping the 
programmer in full control of the staging decisions.

In Scala, type annotations are required on method arguments,
but generally not within method bodies. Thus, we can say
that Scala's local type inference performs 
a semi-automatic binding-time analysis for us,
local within a method body.


% Another crucial aspect: evaluation order.

Following the idea of finally tagless 
\cite{DBLP:journals/jfp/CaretteKS09}
and polymorphic embedding \cite{hoffer08polymorphic}, 
LMS introduces an abstract
type constructor @Rep[T]@ to denote staged expressions
that will generate code of type @T@.

Here is an example -- the ubiquitous @power@ function:
% \begin{lstlisting}
% trait Power { this: Arith =>
%   def power(b: Rep[Double], n: Int): Rep[Double] = 
%     if (n == 0) 1.0 else b * power(b, n - 1)
% }
% \end{lstlisting}
\begin{lstlisting}
val driver = new LMS_Driver[Int,Int] {
  def power(b: Rep[Int], x: Int): Rep[Int] = 
    if (x == 0) 1 else b * power(b, x - 1)

  def main(x: Rep[Int]): Rep[Int] = {
    power(x,4)
  }
}
driver(3)
$\hookrightarrow$ 81
\end{lstlisting}
The @LMS_Driver@ class is provided by the framework, and
we create a new instance @driver@, parameterized to create
a compiled function from @Int@ to @Int@. Inside its scope, we can
use @Rep@ types and the corresponding operations. Method @main@
is the entrypoint for the generated code. At staging time, the driver 
will execute @main@ with a symbolic input. This will completely
evaluate the recursive power invocations (since it is a present-stage
function) and record the individual expression in the IR as they
are encountered. On exit of @main@, the driver will compile
the generated source code and load it as executable into 
the running program. Here, the generated code corresponds to:
\begin{lstlisting}
class Anon12 extends ((Int)=>(Int)) {
  def apply(x0:Int): Int = {
    val x1 = x0*x0
    val x2 = x0*x1
    val x3 = x0*x2
    x3
  }
}
\end{lstlisting}
The performed specializations are immediately clear from the types:
in the definition of @power@, only the base @b@ is 
dynamic (type @Rep[Int]@),
everything else will be evaluated statically, at code generation time.
The expression @driver(3)@ will then execute the generate code, and
return the result 81.



From the view of client code, @Rep@ does not have
any operations of its own. Hence, operations on, e.g., 
@Rep[Int]@ have to be defined externally:
\begin{lstlisting}
trait Base { 
  type Rep[T] 
}
trait IntOps extends Base {
  implicit def unit(x: Int): Rep[Int]
  def infix_+(x: Rep[Int], y: Rep[Int]): Rep[Int]
  def infix_*(x: Rep[Int], y: Rep[Int]): Rep[Int]
}
\end{lstlisting}
Note that from the client's view, all the
operations are abstract. Hence, we can think
of these definitions axiomatizing the 
future-stage language.

For a multi-stage expression such as @b * x@, 
we have tricked Scala's type inference into
performing a simple local binding-time analysis,
which determines which parts of the expression are 
evaluated now and which parts later.
Note that this analysis  is guided by type annotations
present in the code, and hence developers
can more easily reason about the process
as with traditional partial evaluators that
do not make such a distinction.

% \begin{lstlisting}
% def infix_+(x: Rep[Int], y:Rep[Int]): Rep[Int]
% \end{lstlisting}

% \paragraph{Exposed stage distinction}

% Power example:
% \begin{lstlisting}
% ...
% \end{lstlisting}


\paragraph{Virtualization of host language}

If we add booleans, how can we use the normal
@if (c) a else b@ syntax if @c@ has type
@Rep[Boolean]@ instead of @Boolean@? 
The solution is to \emph{virtualize} the host language
\cite{hassan10virtualization}, 
redefining such primitives as method calls:
\begin{lstlisting}
def __ifThenElse[T](c: Rep[Boolean], 
                    a: =>Rep[T], 
                    b: =>Rep[T]): Rep[T]
\end{lstlisting}

While Scala has always had customizable 
@for@ comprehensions in this way, features
like @if/else@ are not part of the standard.
Hence, this syntax requires a fork of the
Scala compiler, called 
Scala-Virtualized \cite{rompf13scalavirt}.

In the signature of @__ifThenElse@ above, it is
important to note the by-name arguments of
type @=>Rep[T]@: these serve to inherit the 
evalation order of the meta-language, as
described next.

\subsection{Multi-stage semantics}

LMS takes care to not duplicate or re-order staged
computations. This is similar to semantics in 
partial evaluation, but unlike quasiquotations in
languages like Lisp or MetaML, which are based
on syntactic expansion. To see the difference,
consider a more sophisticated algorithm to 
compute exponents in logarithmic time by 
repeated squaring:
\begin{lstlisting}
def power(b: Rep[Int], x: Int): Rep[Int] = 
  if (x == 0) 1
  else if ((x&1) == 0) { val y = power(b, x/2); y * y }
  else b * power(b, x - 1)
\end{lstlisting}
The generated code in LMS will be:
\begin{lstlisting}
class Anon13 extends ((Int)=>(Int)) {
  def apply(x0:Int): Int = {
    val x1 = x0+x0
    val x2 = x1*x1
    val x3 = x2*x2
    x3
  }
}
\end{lstlisting}
By contrast, purely syntactic expansion would yield:
\begin{lstlisting}
(((x0+x1)*(x0+x1))*((x0+x1)*(x0+x1)))
\end{lstlisting}
Here, staging has undone the effect of the @val y = ...@
binding and turned our fast algorithm back into
a linear one.
This behavior is widely known, and 
has been studied in the context of MSP languages at 
length \cite{DBLP:conf/pepm/SwadiTKP06}.
While the result is ``only'' a slowdown here,
syntactic expansion and the resulting re-ordering
or duplication of code can be disasterous in the
presence of side effects.

LMS achieves ``proper'' multi-stage semantics by eager
let-insertion \cite{bondorf1990self,DBLP:journals/mscs/HatcliffD97,DBLP:conf/tacs/LawallT97,thiemann1999partial}, i.e.\ binding every new staged operation 
to a fresh identifier as soon as the staged operation is
encountered in the meta-language.


\subsection{Graph-Based IR}

Internally, the LMS API is wired to create an intermediate 
representation (IR) which can be further transformed and 
finally unparsed to target code:
\begin{lstlisting}
trait BaseExp { 
  // IR base classes: Exp[T], Def[T]
  type Rep[T] = Exp[T]
  def reflect[T](x:Def[T]):Exp[T] =.. //add x to IR graph
}
trait IntOpsExp extends BaseExp {
  case class Plus(x:Exp[Int],y:Exp[Int]) extends Def[Int]
  case class Times(x:Exp[Int],y:Exp[Int]) extends Def[Int]
  implicit def unit(x: Int): Rep[Int] = Const(x)
  def infix_+(x:Rep[Int],y:Rep[Int]) = reflect(Plus(x,y))
  def infix_*(x:Rep[Int],y:Rep[Int]) = reflect(Times(x,y))
}
\end{lstlisting}

Another way to look at this structure is as combining a shallow and 
a deep embedding for an IR object 
language \cite{DBLP:journals/cl/SvenningssonA15}.

The fact that we are using (directed, but not necessarily
acyclic) graphs instead of trees enables
comparatively straightforward implementation of 
rather sophisticated optimizations 
\cite{DBLP:conf/jvm/PalecznyVC01,DBLP:journals/toplas/ClickC95}.
Common subexpression elimination is just hash-consing.
Methods like @infix_+@ can serve as smart constructors that perform
optimizations on the fly while building the IR \cite{rompf12optimizing}.
Dead code elimination is just reachability.
These optimization crucially rely on the ability of (effect-free)
nodes to move arbitrarily across the graph.
Before unparsing, a code motion algorithm needs to \emph{schedule}
the graph, and decide in which scope each expression should be
computed.

\medskip
The @LMS_Driver@ class mixes in a number of base traits 
to define the core language:

\begin{lstlisting}
abstract class LMS_Driver[A,B] extends BaseExp 
                                  with IntOpsExp 
                                  with ... 
{
  def main(x: Rep[A]): Rep[B] // abstract
  val apply: (A => B) = {
    // evaluate main to obtain IR
    // unparse IR, compile, and load
  }
}
\end{lstlisting}



\subsection{Horizontal Extensibility}

With this structure, it is now easy to extend
the IR horizontally, by adding new IR node types
and corresponding constructors, for example
for @Boolean@s:

\begin{lstlisting}
trait BooleanOpsExp extends BaseExp {
  case class And(x:Exp[Bool],y:Exp[Bool]) extends Def[Bool]
  case class Or(x:Exp[Bool],y:Exp[Bool]) extends Def[Bool]
  def infix_&(x:Rep[Bool],y:Rep[Bool])=reflectPure(And(x,y))
  def infix_|(x:Rep[Bool],y:Rep[Bool])=reflectPure(Or(x,y))
}
\end{lstlisting}

In the same way, we can add new optimizations via 
smart constructors by overriding the default
term constructor:
\begin{lstlisting}
trait IntOpsExpOpt extends IntOpsExp {
  override def infix_+(x: Exp[Int], y:Exp[Int]) = 
  (x,y) match {
    case (Const(0), y) => y
    case (x, Const(0)) => y
    case _ => super.infix_+(x,y)
  }
}
\end{lstlisting}
Multiple such extensions can be layered,
thanks to the @super@ call in the fall-through
case of the pattern match. Linearization order
of the traits will determine the order in which
the rewrite rules are tried.

All such rewrites are applied eagerly in an 
\emph{online}-fashion while constructing the 
IR, together with all other smart constructors.
As a result, we do not require auxiliary mechanism
such as applying rewrite rules until a fixpoint is reached.
With smart constructors, we directly obtain a strong 
guarantee that, in this case, no IR node of the form @0 + y@ or
@x + 0@ will ever be created.

% \note{
%   Sec 2.4. I'm wondering if deciding on the order of applying the
% rewriting rules (the order of stacking up traits) turns out a
% problem. In is well-known from theory and the compiler optimization
% folklore that the order of various simplifications matters a great
% deal. For example, the expression

%        val x = (y + 1) + (-1)

% will be simplified only when the zero suppression pass is performed
% after the re-association and constant propagation. The expression may
% even be left unsimplified, if the reassociation is not performed --
% and how do we know how to apply associativity and commutativity rules?

% Also, what if we want to apply a series of re-writing rules (expressed
% in traits) several (statically unknown number of) times, until a
% fixpoint is reached or the `fuel' has run out? Wouldn't the type-based
% nature of re-writing get in the way?
% }


\subsection{Vertical Extensibility}

Horizontal extensibility and optimizations
with smart constructors go a long way,
but in some cases we need additional
power. In particular, it is often necessary
to first perform rewrites on a high-level of
abstraction, say, matrices and vectors in 
linear algebra, which would be implemented
in the same way as the plus operator on integers shown above. 
But we also want staging to transform matrix products
into tight loops \emph{after} high-level
algebraic optimizations are applied
exhaustively \cite{rompf12optimizing}.

% \note{
% I was thrilled to read about "high-level rewrites" in the first 
%   paragraph of Sec. 2.5, but was disappointed that no such example is presented 
%   in the section (or it is somehow hidden in the code and I didn't get it)
% }

For such use cases, LMS provides a
traversal and transformer API:
\begin{lstlisting}
trait LowerVectors extends ForwardTransformer {
  val IR: VectorExp; import IR._
  def transformDef[T](d: Def[T]): Exp[T] = d match {
    case VectorZeros(n) => 
      vec_zeros_ll(transformExp(n))
    case VectorPlus(a,b) => 
      vec_plus_ll(transformExp(a), transformExp(b))
    case _ => super.transformDef(d)
  }
}
\end{lstlisting}
Here, @vec_zeros_ll@ and @vec_plus_ll@ are
taken to be methods defining how zero
vectors and vector addition are represented
on a lower-level, using just arrays and
loops:
\begin{lstlisting}
def vec_zeros_ll[T:Numeric](n: Rep[Int]): Rep[Vec[T]] =
  Vector.fromArray(Array.fill(n) { i => zero[T] })
def vec_plus_ll[T:Numeric](a:Rep[Vec[T]],b:Rep[Vec[T]])=
  Vector.fromArray(a.data.zipWith(b.data)(_ + _))
\end{lstlisting}

The definitions of these methods can use staging
again on the lower level, and hence the overall
pattern corresponds to implementing translation
passes as \emph{staged interpreters} over
the higher-level IR.

If we want to use the transformers API with custom
IR nodes, we need to implement a default 
transformation case called \emph{mirroring} for 
each of them:
\begin{lstlisting}
// mirror: transformation default case
def mirror[T](d: Def[T])(t: Transformer) = d match {
  case VectorZeros(n) => 
    Vector.zeros(t.transformExp(n))
  case VectorPlus(a,b) => 
    t.transformExp(a) + t.transformExp(b)
  case _ => super.mirror(d)
}
\end{lstlisting}




\section{Practical Issues}\label{sec:problems}

Here are some issues encountered in practice
with the current design.

% - Sec. 3, "Non-problem": My take-away from this section was "there is a problem 
%   which is not a problem". Not very enlightening.

\paragraph{Non-problem: Rep types} Sometimes
@Rep@ types themselves are pointed out as an issue, 
or the fact that they show up in type errors even though
they might have been inferred in the source.
We do not consider this an actual problem, but rather 
a symptom of unfamiliarity. 
In our experience with new LMS users, in particular students,
these issues disappear once an initial understanding
of the different stages of execution sets in. From that
point on, @Rep@ types seem to strike a good balance in 
terms of noise, and type errors
are no more complicated (and often less) than with other 
Scala libraries that rely on advanced types.

% \note{
% Sec 3, Rep type non-problem. I'm concerned about the phrase `our
% experience'. Whose experience is that, exactly? LMS developers, Delite
% developers, users of LMS-embedded languages, new LMS users, etc? What
% is not a problem for LMS or Delite developers could still be a
% stumbling block for others.
% }

\paragraph{Conversions to/from Rep}

The distinction between normal Scala types
and staged @Rep@ types sometimes leads to
inconvenient conversions. Assume we declare 
a user-defined class of complex numbers:
\begin{lstlisting}
case class Complex(re:Rep[Double],im:Rep[Double])
\end{lstlisting}
Since @Complex@ is not a @Rep@ type, we cannot 
easily return complex numbers from an
@if@ expression. To do so, we would either need
to support complex numbers as first-class type 
@Rep[Complex]@ in our IR, or we would need to convert 
them into a tuple @Rep[(Double,Double)]@ and back.
The issue gets worse if we have a whole class
hierarchy, e.g.\ @Complex@ as an abstract base
class with subclasses @Polar@ and @Cartesian@,
which necessitates a uniform encoding of all 
possible alternatives, e.g.\ as tagged union
\cite{rompf12optimizing}.

% \note{- Sec. 3, Conversions to/from Rep: Why can't you have Rep[Complex]?}

% \note{Sec 3, to/from Rep conversion. It is indeed easy to convert between
% (Rep Int, Rep Int) and Rep (Int,Int) -- this is just
% eta-conversion for products. Sums, on the other hand, pose a
% significant problem. Can you comment?
% }

\paragraph{GADT Pattern matching}

Smart constructors or transformers rely on
pattern matching over typed IR nodes, which
are implemented as (Scala's approximation of)
GADTs. Unfortunately, Scala's GADT support
in pattern matching is incomplete. As a result,
it is often required to insert type casts.
For example, the definition of @mirror@
above will actually look like:
\begin{lstlisting}
def mirror[T](d: Def[T])(t: Transformer) = (d match {
  case VectorZeros(n) => ...
}).asInstanceOf[Exp[T]]
\end{lstlisting}
This necessity for explicit casts somewhat
negates the benefits of a typed IR and
makes pattern matching fundamentally
unsafe.

% def beta[T]: Exp[T] => Exp[T] = { 
%   case App(f: Fun[t0, T], a) => 
%     f.lam(beta(a.asInstanceOf[Exp[t0]]))
% }

% \note{- Sec. 3, GADT pattern matching: What is the reason for the incompleteness? 
%   Is that a principal limitation of Scala? Can it be fixed?
% }

\paragraph{Boilerplate for DSL definition}

Defining custom IR nodes incurs a fair bit
of boilerplate.
We first need to provide the interface trait 
with abstract methods for all operations.
Then we need to provide the implementation 
trait, with @Def@ classes and implemented methods. 
We may need additional traits for optimizations,
again with overridden methods. And finally we
need to implement mirroring for each @Def@ 
class.

\paragraph{Two-thirds-solution: Forge} 
While this boilerplate is repetitive, it
follows a clear structure, and often large
parts can be auto-generated. 
Forge \cite{gpce13sujeeth} is a meta-DSL
that takes a declarative specification as
input, and generates all the necessary LMS
definitions.

But there is still a problem: even though
the boilerplate no longer needs to \emph{written},
the Scala compiler still needs to deal with
all the complexity.

\paragraph{Problem: Scala compile time (1)} 

The embedding using @Rep[T]@ means that we cannot 
use normal Scala methods for staged functionality.
For example, an operation @Rep[Int] + Rep[Int]@
cannot be implemented as a method on type @Rep@
or @Int@.
We could use implicits, but they are imprecise, 
in particular for operations involving numeric types.
A key problem is to model the right widening
behavior for @Rep[Float] + Rep[Double]@,
@Int + Rep[Int]@ and so on.

% \note{- Sec. 3, Problem: Scala compile time(1): "cannot use normal methods for staged 
%   functionality" - what is "staged functionality"? The example didn't help.
% }


Scala-Virtualized implements @infix_@ methods, 
which enable more precision. But leveraging this 
precision requires spelling out many alternatives: 
more than 30 for @+@ in Delite, with @{Int,Long,Float,Double}@ x @{T,Rep}@ x @{Scalar,Vector}@.
% In addition, String plus. And some generic versions,
% which even triggered implicit lookup.
This large set of alternatives leads to a drastic
slow-down, since overloading resolution has $O(n^2)$ complexity
in @scalac@.
In Delite, type checking a single @+@ could take up
to 0.5s. A notable worst case was an unrolled determinant 
calculation with 33 plus operations that took a good 
minute to type check.

\paragraph{Problem: Scala compile time (2)} 

Another issue with compile times is due to
mixin composition of a large number of traits,
which each contain a large number of members.
To compile traits into JVM classes, the Scala 
compiler needs to generate bridge methods and 
other auxiliary definition. And when mixing
traits together, the compiler needs to check 
for overriding pairs, which is again an
$O(n^2)$ operation in the worst case.


\section{Type-Based Embedding}\label{sec:one}

Going back to our starting point, we realize
that we do not have to use @Rep[T]@ to realize
a type-based embedding. It is enough to
have \emph{some} distinction between, say,
the normal Scala @Int@ and the staged @Int@.
In particular, we can just define a new
@Int@ type:

\begin{lstlisting}
package dsl
trait Int {
  def +(y:Int): Int
  def *(y:Int): Int
}
\end{lstlisting}

And rely on scoping and path-dependent types
to distinguish @scala.Int@ and @dsl.Int@.

But how can we tell on a more abstract level
whether a given type is staged or not?
The key idea here is to use type classes:
we require an instance of a type class
@Typ[T]@ whenever @T@ is a staged type.

\note{- Sec. 4, "whenever T is a staged type" - what does this mean? I thought 
  LMS provides fine-grained staging annotations through type annotations and 
  "staged" is not a property of types.}


Putting these ideas together, we come up with
the following alternative definition of
the core LMS traits seen before:

\begin{lstlisting}[mathescape=false]
trait Base {
  // preliminaries
  @implicitNotFound("${T} is not a DSL type")
  type Typ[T]
}
trait IntOps extends Base {
  trait IntBase {
    def +(y: Int): Int
    def *(y: Int): Int
  }
  type Int <: IntBase
  implicit def intTyp: Typ[Int]
}
\end{lstlisting}

Again the interface is purely abstract,
achieving separation of concerns, and
enabling us to pick a suitable IR
representation of our choice.

% \note{
% - Sec. 4, I found the example too short and I didn't get how the novel 
%   technique is supposed to work.  For instance, it would have helped to see 
%   the power function and the driver from Sec. 2.1 rephrased in the new design.}

Generic code now requires a @Typ[T]@ instance 
instead of using @Rep@ types. For example:
\begin{lstlisting}
def __ifThenElse[T:Typ](c:Boolean, a: =>T, b: =>T): T
\end{lstlisting}
Note that we take @Boolean@ to be a staged boolean
here, different from the normal @scala.Boolean@,
and implemented in the same way as the staged
@Int@ type above.

% \note{
%   - Sec. 4, "Note that we take Boolean to be a staged boolean here" - where do 
%   I see that it is a staged boolean?
% }

As a pleasant side effect, the use of @implicitNotFound@ 
leads to better error messages than higher-kinded types.

For user-facing code, we now have a couple of
choices, depending on the desired noise level.
Instead of going with overloaded @Int@ or
@Boolean@, we could also pick unique name 
for types (e.g., @DInt@) or we use qualified
prefixes like @IR.Int@. Either way, using 
imports and scoping we can define whether 
@Int@ resolves to @IR.Int@ or @scala.Int@
by default.

\note{To complete this sections, we show three
possible ways of writing the power example again:}

\begin{lstlisting}
val driver = new LMS_Driver[Int,Int] {
  def power(b: Int, x: scala.Int): Int = 
    if (x == 0) 1 else b * power(b, x - 1)

  def main(x: Int): Int = {
    power(x,4)
  }
}
\end{lstlisting}
\note{TODO}


\section{Virtualization using Macros}\label{sec:two}

Instead of having to add all @Int@ methods to @Rep[Int]@
externally, we now use normal objects and classes.
Everything is still implemented purely as a library,
and we do not require any other mechanisms such as
macros \emph{per se}.

Since we use normal objects and classes for staged
types, we do not need infix methods anymore,
and thus the first compiler performance problem goes away.

Some other things become simpler as well. The Scala 
virtualized compiler had special support for
staged structural types via @Struct[T]@. 
This is also no longer needed. We just
need a macro that creates a @Typ[T]@ instance for
structural types @T <: Struct@ where all fields
have an implicit @Typ@ instance available.

The remaining functionality, like virtualizing
built-ins such as @if (c) a else b@ as @__ifThenElse(c,a,b)@,
can easily be implemented using annotation
macros, obviating the need for a dedicated
compiler fork.


\section{Graph-Based IR}\label{sec:three}

As a final piece in the puzzle, we can use the new
embedding with an \emph{untyped} IR, based 
essentially on S-expressions.

\begin{lstlisting}
trait BaseExp extends Base {
  trait Typ[T] {
    def decode(e:Exp): T
    def encode(x:T): Exp
  }
}
trait IntOpsExp extends BaseExp with DSL {
  case class Int(e: Exp) extends IntBase {
    def +(y: Int) = reflect[Int]("int-+", e, y.e)
    def *(y: Int) = reflect[Int]("int-*", e, y.e)
  }
  implicit val intTyp: Typ[Int] = new Typ[Int] { 
    def decode(e:Exp) = Int(e); 
    def encode(x:Int) = x.e;
    override def toString = "Int" 
  }
}
\end{lstlisting}
The @encode@ and @decode@ methods in @Typ[T]@
are required to convert between the user-facing
and the IR-internal representation, suitable
for unparsing.

\note{Sec 4. Defining a new Int type and the operations on them is indeed
straightforward. But what about functions, for example? We don't want
make arrow types the members of Typ, do we? What about parameterized
types such as list constructors or various collection types,
especially generic collections?
}


This removes the need for 100s of classes for
different IR nodes, leads to fewer generated bytecode, 
and faster compile times, eliminating the second
performance problem.

How can we add new types in this model? It's simple,
we just create a new class and provide a @Typ[T]@
instance.
How can we add new operations to an existing
type? We use normal Scala implicit classes.

How can we add new optimizations? We provide
a new facility:
\begin{lstlisting}
rewrite { v1: Vector => 
  (v1 + 0) ===> v1
}
\end{lstlisting}
Method @rewrite@ can treat the passed closure
as higher-order abstract syntax, and invoke it
with a fresh symbol as @v1@. 
Both sides of the @===>@ arrow are now S-expressions,
so internally, @rewrite@ will obtain a reified 
representation of the rewrite rule!

\note{Note that the arrow ===> maps to an S-expression, too.}

How can we add new transformations? Using the same
new mechanism:
\begin{lstlisting}
lower { v1: Vector => 
  v1 ===> v1.data
}
\end{lstlisting}

With further uses of annotation macros we can even
automate this process, and derive IR definitions
as well as lowering transformations directly from
a class definition. For this purpose, we introduce
a macro @≠ir@, which, used as follows:
\begin{lstlisting}
@ir class Vector(data: Array[Int]) {
  def +(o: Vector) = 
    new Vector((data zip o.data) map (_ + _))
}
\end{lstlisting}
Expands the class definition to the following:
\begin{lstlisting}
class Vector(data: Array[Int]) {
  def +(o: Vector) = 
    reflect[Vector]("vector-+", this, o)
  def +_body(o: Vector) = 
    new Vector((data zip o.data) map (_ + _))

}
lower { v1: Vector, v2: Vector => 
  (v1 + v2) ===> (v1.+_body(v2))
}
\end{lstlisting}

\note{reflect not defined}

In summary, even though the IR is now untyped, 
we can still specify rewrites and transformations
in a typed way, and get the benefits of 
horizontal and vertical extensibility.

\note{TODO: generics
One challenge in this model is dealing with
generic types, e.g., a class Vector[T].
We solve it through Generic.T1, .T2, ...}


\note{TODO: stage polymorphism
How to abstract over staging? Can still have the
split in interface and implementation traits:
// example
}

\note{Sec 6: the untyped IR is indeed no obstacle for typeful IR
transformations, so we can still get the benefit of types in writing
the transformations. However, before (Sec 2.5) we could get the benefit of
types when stacking up transformations vertically, via ``staged
interpreters of higher-level [but still typed] IR.'' Now, with IR
being untyped, staged IR interpreters can no longer be typed, can
they?
}


